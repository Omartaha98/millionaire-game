package com.example.m.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.media.MediaPlayer;
public class Main2Activity extends AppCompatActivity {
    MediaPlayer ring= MediaPlayer.create(Main2Activity.this,R.raw.start);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        //ring.start();
        Button next = findViewById(R.id.go);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent x =new Intent(Main2Activity.this,MainActivity.class);
                startActivity(x);

            }
        });

    }

}
