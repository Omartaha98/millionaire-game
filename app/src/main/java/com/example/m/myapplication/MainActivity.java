package com.example.m.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Random;
import java.util.Vector;


public class MainActivity extends AppCompatActivity {

    Random Question = new Random();
    public int c = 0;
    public int count =0;
    public int x1=1;
    public int x2=1;
    public int x3=1;
    public int x4=1;
    public int i=0;
    public int random=15;

    public class point
    {
        String q,A1,A2,A3,A4,RA;
    };
    ArrayList<point>array= new ArrayList<point>();
    public void insert() {
        TextView q11 = findViewById(R.id.question);
        q11.setMovementMethod(new ScrollingMovementMethod());
        Button A11 = findViewById(R.id.A1);
        Button A22 = findViewById(R.id.A2);
        Button A33 = findViewById(R.id.A3);
        Button A44 = findViewById(R.id.A4);


            if(i<5){
                count = Question.nextInt(random);
                q11.setText(array.get(count).q);
                A11.setText(array.get(count).A1);
                A22.setText(array.get(count).A2);
                A33.setText(array.get(count).A3);
                A44.setText(array.get(count).A4);
                random--;
            }

            else if(i<10&&i>=5){
                random+=5;
                count = 10+Question.nextInt(random);
                q11.setText(array.get(count).q);
                A11.setText(array.get(count).A1);
                A22.setText(array.get(count).A2);
                A33.setText(array.get(count).A3);
                A44.setText(array.get(count).A4);
                random--;
            }
            else if(i<15&&i>=10){

                count = 20+Question.nextInt(15);
                q11.setText(array.get(count).q);
                A11.setText(array.get(count).A1);
                A22.setText(array.get(count).A2);
                A33.setText(array.get(count).A3);
                A44.setText(array.get(count).A4);
            }

    }
    public  void price()
    {
        if(i==0)
        {
            startActivity(new Intent(MainActivity.this, Pop1.class));
            i++;
            array.remove(count);
            //count++;
            insert();

        }
        else if(i==1)
        {
            startActivity(new Intent(MainActivity.this, Pop.class));
            i++;
            array.remove(count);
            //count++;
            insert();

        }
        else if(i==2)
        {
            startActivity(new Intent(MainActivity.this, Pop2.class));
            i++;
            array.remove(count);
            //count++;
            insert();

        }
        else if(i==3) {
            startActivity(new Intent(MainActivity.this, Pop3.class));
            i++;
            array.remove(count);
            //count++;
            insert();


        }
        else if(i==4) {
            startActivity(new Intent(MainActivity.this, Pop4.class));
            i++;
            array.remove(count);
            //count++;
            insert();

        }

        else if(i==5)
        {
            startActivity(new Intent(MainActivity.this,Pop5.class));
            i++;
            array.remove(count);
            //count++;
            insert();

        }
        else if(i==6){
            startActivity(new Intent(MainActivity.this,Pop6.class));
            i++;
            array.remove(count);
            //count++;
            insert();

        }
        else  if (i==7){
            startActivity(new Intent(MainActivity.this,Pop7.class));
            i++;
            array.remove(count);
            //count++;
            insert();

        }
        else if(i==8){
            startActivity(new Intent(MainActivity.this,Pop8.class));
            i++;
            array.remove(count);
            //count++;
            insert();

        }
        else if(i==9){
            startActivity(new Intent(MainActivity.this,Pop9.class));
            i++;
            array.remove(count);
            //count++;
            insert();

        }
        else if(i==10){
            startActivity(new Intent(MainActivity.this,Pop10.class));
            i++;
            array.remove(count);
            //count++;
            insert();

        }
        else if(i==11){
            startActivity(new Intent(MainActivity.this,Pop11.class));
            i++;
            array.remove(count);
            //count++;
            insert();

        }
        else if(i==12){
            startActivity(new Intent(MainActivity.this,Pop12.class));
            i++;
            array.remove(count);
            //count++;
            insert();

        }
        else if(i==13){
            startActivity(new Intent(MainActivity.this,Pop13.class));
            i++;
            array.remove(count);
            //count++;
            insert();

        }
        else if(i==14) {
            startActivity(new Intent(MainActivity.this,Pop14.class));
            finish();
            moveTaskToBack(true);
            i++;
            array.remove(count);
            //count++;
            insert();

        }
    }
    public  void lose()
    {
        if(i<5)
        {
            startActivity(new Intent(MainActivity.this,Pop16.class));

        }
        else if (i>=5&&i<14) {
            startActivity(new Intent(MainActivity.this, Pop17.class));
        }
        else if(i==14){
            startActivity(new Intent(MainActivity.this, Pop18.class));
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        point q1 = new point();point q2 = new point();
        point q3 = new point();point q4 = new point();
        point q5 = new point();point q6 = new point();
        point q7 = new point();point q8 = new point();
        point q9 = new point();point q10 = new point();
        point q11 = new point();point q12 = new point();
        point q13 = new point();point q14 = new point();point q15 = new point();
        point q16 = new point();
        point q17 = new point();point q18 = new point();point q19 = new point();point q20 = new point();
        point q21 = new point();point q22 = new point();point q23 = new point();point q24 = new point();
        point q25 = new point();point q26 = new point();point q27 = new point();point q28 = new point();
        point q29 = new point();point q30 = new point();point q31 = new point();point q32 = new point();
        point q33 = new point();point q34 = new point();point q35 = new point();point q36 = new point();
        point q37 = new point();point q38 = new point();point q39 = new point();point q40 = new point();
        point q41 = new point();point q42 = new point();point q43 = new point();point q44 = new point();point q45 = new point();
        q1.q="Dr Martin the chemist is weighing out some pills. He has some 5g weights and some 7g weights.Can he weigh exactly 38g of pills?";
        q1.A1="(three 7g and three 5g and one 1g weights)";
        q1.A2="(four 7g and two 5g weights)";
        q1.A3="(two 7g and foyer 5g and one 4g weights)";
        q1.A4="(all the previous)";
        q1.RA="(four 7g and two 5g weights)";
        array.add(q1);
        q2.q=" what is the 13th term of this sequence : 0,1,1,2,5,8,13,21,34,55 ?";
        q2.A1="(233)";
        q2.A2="(377)";
        q2.A3="(144)";
        q2.A4="(none of the previous)";
        q2.RA="(233)";
        array.add(q2);
        q3.q = "frog wants to get out of a well with height 99 meters,\\n every day this frog can climb 7 meters and fall down 3 meters.\\n How many days will it take the frog to get out of the well ?";
        q3.A1 = "(26)";
        q3.A2 = "(25)";
        q3.A3 = "(24)";
        q3.A4 = "(23)";
        q3.RA = "(24)";
        array.add(q3);
        q4.q = "what is the angle between the hours bar and the minutes one.The time back then was : 12 : 30 PM.?";
        q4.A1 = "(180)";
        q4.A2 = "(165)";
        q4.A3 = "(168)";
        q4.A4 = "(162)";
        q4.RA = "(165)";
        array.add(q4);
        q5.q = "There are 2 brothers among a group of 20 persons. In how many ways can the group be arranged around a circle so that there is exactly one person between the two brothers?";
        q5.A1 = "2 * 19!";
        q5.A2 = "18!* 18";
        q5.A3 = "19!* 18";
        q5.A4 = "2 * 18!";
        q5.RA = "2 * 18!";
        array.add(q5);
        q6.q = "How many ways can 4 prizes be given away to 3 boys, if each boy is eligible for all the prizes?";
        q6.A1 = "256";
        q6.A2 = "12";
        q6.A3 = "81";
        q6.A4 = "None of these";
        q6.RA = "81";
        array.add(q6);
        q7.q = "There are 12 yes or no questions. How many ways can these be answered?";
        q7.A1 = "1024";
        q7.A2 = "2048";
        q7.A3 = "4096";
        q7.A4 = "144";
        q7.RA = "4096";
        array.add(q7);
        q8.q = "In how many ways can the letters of the word EDUCATION be rearranged so that the relative position of the vowels and consonants remain the same as in the word EDUCATION?";
        q8.A1 = "9!/ 4";
        q8.A2 = "9!/ (4!* 5!)";
        q8.A3 = "4!* 5!";
        q8.A4 = "None of these";
        q8.RA = "4!* 5!";
        array.add(q8);
        q9.q = "Ram covers a part of the journey at 20 kmph and the balance at 70 kmph taking total of 8 hours to cover the distance of 400 km. How many hours has been driving at 20 kmph?";
        q9.A1 = "2 hours";
        q9.A2 = "3 hours 20 minutes";
        q9.A3 = "4 hours 40 minutes";
        q9.A4 = "3 hours 12 minutes";
        q9.RA = "3 hours 12 minutes";
        array.add(q9);
        q10.q = "What is the greatest number of five which exactly divisible by 7, 10, 15, 21 and 28 ?";
        q10.A1 = "99840";
        q10.A2 = "99900";
        q10.A3 = "99960";
        q10.A4 = "99990";
        q10.RA = "99960";
        array.add(q10);
        q11.q = "What is the minimum number of square marbles required to tile a floor of length 5.78m and width 3.74m ?";
        q11.A1 = "176";
        q11.A2 = "187";
        q11.A3 = "54043";
        q11.A4 = "748";
        q11.RA = "187";
        array.add(q11);
        q12.q = "How many numbers from 1 to 20000 are divisible by all numbers from 2 to 10?";
        q12.A1 = "5";
        q12.A2 = "150";
        q12.A3 = "7";
        q12.A4 = "300";
        q12.RA = "7";
        array.add(q12);
        q13.q="Leap year is a year divisible by 4 and not divisible by 100 with exception that years that are divisible by 400 are also leap year.\\n What of the following is leap year ?";
        q13.A1="404";
        q13.A2="440";
        q13.A3="240";
        q13.A4="no right answer";
        q13.RA="404";
        array.add(q13);
        q14.q="Sophia finished 2/3 of a book she calculated that she go more pages then she has yet to read.\\n How many is her book? ";
        q14.A1="260";
        q14.A2="270";
        q14.A3="310 ";
        q14.A4="275";
        q14.RA="275";
        array.add(q14);
        q15.q="Sphere of radius r1 how many cylinders of r2 and height h can be made using the sphere? \\ nWhen r1=5, r2=2, h=3";
        q15.A1="15";
        q15.A2="17";
        q15.A3="can’t";
        q15.A4="13";
        q15.RA="13";
        array.add(q15);
        q16.q="This is the pseudo code of : \\na-First it finds the smallest element in the array.\\nb- Exchange that smallest element with the element at the first position.\\nc-Then find the second smallest element and exchange that element withthe element at the second position.\\nd-This is process continues until the complete array is sorted.";
        q16.A1="(bubble sort)";
        q16.A2="(selection sort)";
        q16.A3="(merge sort)";
        q16.A4="(none of the previous)";
        q16.RA="(selection sort)";
        array.add(q16);
        q17.q="what is the output of int a = 3, b = 6, years = 0;  ? \\n  while (a <= b)\\n   {\\n    a *= 3;\\n    b *= 2;\\n    years++;\\n   }\\n  cout << years << endl;";
        q17.A1="(4)";
        q17.A2="(3)";
        q17.A3="(2)";
        q17.A4="dinfinite loop";
        q17.RA="(2)";
        array.add(q17);
        q18.q="If we have 8 pens numbered from 1 to 8, all of them are blue \\n except one is red but all of them are covered in black.the red pen is heavier \\n than any blue pen, but pens weights are so negligible that humans can't actually feel the difference.\\n and asked you to get the red pen, You can ask one type of questions any number of times. \\n The question you may ask is, which hand holds heavier group ? \\n (You can hold any number of the 8 pens in one hand and any number of the remaining pens in the other hand\\n and ask the question and you will get on the correct answer).\\n What is the minimum number of questions to know the red pen?";
        q18.A1="(2)";
        q18.A2="(3)";
        q18.A3="(4)";
        q18.A4="(5)";
        q18.RA="(2)";
        array.add(q18);
        q19.q="Moana set her digital watch at 13:00:00 on the last day of April. Unfortunately the watch loses 11 seconds a day.\\n What is the time on Moana’s watch when it is 13:00 : 00 on the last day of May ?";
        q19.A1="(12:00:00)";
        q19.A2="(12:55 : 21)";
        q19.A3="(12:54 : 00)";
        q19.A4="(12:54 : 19)";
        q19.RA="(12:54 : 19)";
        array.add(q19);
        q20.q="Train A traveling at 60 km/hr leaves Mumbai for Delhi at 6 P.M. Train B traveling at 90 km/hr also leaves Mumbai for Delhi at 9 P.M. Train C leaves Delhi for Mumbai at 9 P.M. If all three trains meet at the same time between Mumbai and Delhi, what is the speed of Train C if the distance between Delhi and Mumbai is 1260 kms?";
        q20.A1="60 km / hr";
        q20.A2="90 km / hr";
        q20.A3="120 km / hr";
        q20.A4="135 km / hr";
        q20.RA="120 km / hr";
        array.add(q20);
        q21.q="If the wheel of a bicycle makes 560 revolutions in travelling 1.1 km, what is its radius?";
        q21.A1="31.25 cm";
        q21.A2="37.75 cm";
        q21.A3="35.15 cm";
        q21.A4="11.25 cm";
        q21.RA="31.25 cm";
        array.add(q21);
        q22.q="A number is selected at random from first thirty natural numbers. What is the chance that it is a multiple of either 3 or 13?";
        q22.A1="17 / 30";
        q22.A2="2 / 5";
        q22.A3="11 / 30";
        q22.A4="4 / 15";
        q22.RA="2 / 5";
        array.add(q22);
        q23.q="From the following choices what is the equation of a line whose x intercept is half as that of the line 3x + 4y = 12 and y intercept is twice as that of the same line?";
        q23.A1="3x + 8y = 24";
        q23.A2="8x + 3y = 24";
        q23.A3="16x + 3y = 24";
        q23.A4="3x + y = 6";
        q23.RA="3x + y = 6";
        array.add(q23);
        q24.q="How many digits will the number 3200 have if the value of log 3 = 0.4771 ?";
        q24.A1="95";
        q24.A2="94";
        q24.A3="96";
        q24.A4="None of these";
        q24.RA="96";
        array.add(q24);
        q25.q="10-Given an integer A and definning f(K)=1+[A-k/(k+1)f(k+1)].\\nKnowing that this is a non - stopping recursion!!.Can you finf the value of f(0) if A = 5 ?";
        q25.A1="208 / 63";
        q25.A2="316 / 17";
        q25.A3="115 / 12";
        q25.A4="13 / 6";
        q25.RA="208 / 63";
        array.add(q25);
        q26.q="If X,Y,Z are consecutive negative integers and if X>Y>Z.\\nWhich of the following must be odd positive integer ?";
        q26.A1="XYZ";
        q26.A2="(X - Y)(Y - Z)";
        q26.A3="X(Y + Z)";
        q26.A4="X - Y - Z";
        q26.RA="(X - Y)(Y - Z)";
        array.add(q26);
        q27.q="Given A=265 and B=(264+263+262+....+20), then:";
        q27.A1="B is 264 larger than A";
        q27.A2="A and B are equal";
        q27.A3="B is larger than A by 1";
        q27.A4="A is larger than B by 1";
        q27.RA="A is larger than B by 1";
        array.add(q27);
        q28.q="Three runners A,B and C run a race where runner A finishing 12m ahead of runner B and 18m ahed of runner C,while runner B finishing 8m ahead of runner C.\\nEach runner travels the entire distance at a constant speed.What was the length of the race ?";
        q28.A1="36m";
        q28.A2="48m";
        q28.A3="60m";
        q28.A4="72m";
        q28.RA="48m";
        array.add(q28);
        q29.q="A rectangular shape with the size n?×?m meters. Each flagstone is of the size a?×?a.What is the least number of flagstones needed to pave the Square ?\\nIt's allowed to cover the surface larger than the Theatre Square, but the Square has to be covered.It's not allowed to break the flagstones.\\nThe sides of flagstones should be parallel to the sides of the Square.12, 13, 4 ?";
        q29.A1="12";
        q29.A2="15";
        q29.A3="10";
        q29.A4="13";
        q29.RA="12";
        array.add(q29);
        q30.q="Sphere of radius r1 how many cylinders of r2 and height h can be made using the sphere?\\nWhen r1 = 5, r2 = 2, h = 3";
        q30.A1="15";
        q30.A2="17";
        q30.A3="can’t";
        q30.A4="13";
        q30.RA="13";
        array.add(q30);
        q31.q="What is the output of this program?\\n#include <iostream>\\n#include <algorithm>\\n#include <vector>\\nusing namespace std;\\nint main()\\n{\\nint myints[] = { 10, 20, 30, 5, 15 };\\nvector<int> v(myints, myints + 5);\\nmake_heap(v.begin(), v.end());\\npop_heap(v.begin(), v.end()); v.pop_back();\\nv.push_back(99); push_heap(v.begin(), v.end());\\nsort_heap(v.begin(), v.end());\\nfor (unsigned i = 0; i < v.size(); i++)\\n\\tcout << ' ' << v[i];\\nreturn 0;\\n}";
        q31.A1="5 10";
        q31.A2="5 10 15 20";
        q31.A3="5 10 15 20 99";
        q31.A4="None of the mentioned";
        q31.RA="5 10 15 20 99";
        array.add(q31);
        q32.q="If a simple graph G, contains n vertices and m edges, the number of edges in the Graph G'(Complement of G) is ___________";
        q32.A1="(n*n - n - 2 * m) / 2";
        q32.A2="(n*n + n + 2 * m) / 2";
        q32.A3="(n*n - n - 2 * m) / 2";
        q32.A4="(n*n - n + 2 * m) / 2";
        q32.RA="(n*n - n - 2 * m) / 2";
        array.add(q32);
        q33.q="For which of the following combinations of the degrees of vertices would the connected graph be eulerian?";
        q33.A1="1, 2, 3";
        q33.A2="2, 3, 4";
        q33.A3="2, 4, 5";
        q33.A4="1, 3, 5";
        q33.RA="1, 2, 3";
        array.add(q33);
        q34.q="For which of the following inputs would Kadane’s algorithm produce the CORRECT output?";
        q34.A1="{0, 1, 2, 3}";
        q34.A2="{-1, 0, 1}";
        q34.A3="{-1, -2, -3, 0}";
        q34.A4="All of the mentioned";
        q34.RA="All of the mentioned";
        array.add(q34);
        q35.q="Consider the matrices P, Q and R which are 10 x 20, 20 x 30 and 30 x 40 matrices respectively. What is the minimum number of multiplications required to multiply the three matrices?";
        q35.A1="18000";
        q35.A2="12000";
        q35.A3="24000";
        q35.A4="32000";
        q35.RA="18000";
        array.add(q35);
        q36.q="Consider the 2×3 matrix {{1,2,3},{1,2,3}}. What is the sum of elements of the maximum sum rectangle?";
        q36.A1="3";
        q36.A2="6";
        q36.A3="12";
        q36.A4="18";
        q36.RA="12";
        array.add(q36);
        q37.q="Consider the recursive implementation to find the nth fibonacci number:\\nint fibo(int n)\\nif n <= 1\\nreturn n\\nreturn __________;\\nWhich line would make the implementation complete ?";
        q37.A1="fibo(n) + fibo(n)";
        q37.A2="fibo(n) + fibo(n – 1)";
        q37.A3="fibo(n – 1) + fibo(n + 1)";
        q37.A4="fibo(n – 1) + fibo(n – 2)";
        q37.RA="fibo(n – 1) + fibo(n – 2)";
        array.add(q37);
        q38.q="What is the number of moves required in the Tower of Hanoi problem for k disks?";
        q38.A1="2k – 1";
        q38.A2="2k + 1";
        q38.A3="2k + 2";
        q38.A4="2k – 1";
        q38.RA="2k – 1";
        array.add(q38);
        q39.q="Suppose you have coins of denominations 1, 3 and 4. You use a greedy algorithm, in which you choose the largest denomination coin which is not greater than the remaining sum.\\n For which of the following sums, will the algorithm NOT produce an optimal answer?";
        q39.A1="20";
        q39.A2="12";
        q39.A3="6";
        q39.A4="5";
        q39.RA="6";
        array.add(q39);
        q40.q="Consider the matrices P, Q and R which are 10 x 20, 20 x 30 and 30 x 40 matrices respectively. What is the minimum number of multiplications required to multiply the three matrices?";
        q40.A1="18000";
        q40.A2="12000";
        q40.A3="24000";
        q40.A4="32000";
        q40.RA="18000";
        array.add(q40);
        q41.q="Consider the 2×3 matrix {{1,2,3},{1,2,3}}. What is the sum of elements of the maximum sum rectangle?";
        q41.A1="3";
        q41.A2="6";
        q41.A3="12";
        q41.A4="18";
        q41.RA="12";
        array.add(q41);
        q42.q="Consider the recursive implementation to find the nth fibonacci number:\\nint fibo(int n)\\nif n <= 1\\nreturn n\\nreturn __________;\\nWhich line would make the implementation complete ?";
        q42.A1="fibo(n) + fibo(n)";
        q42.A2="fibo(n) + fibo(n – 1)";
        q42.A3="fibo(n – 1) + fibo(n + 1)";
        q42.A4="fibo(n – 1) + fibo(n – 2)";
        q42.RA="fibo(n – 1) + fibo(n – 2)";
        array.add(q42);
        q43.q="What is the number of moves required in the Tower of Hanoi problem for k disks?";
        q43.A1="2k – 1";
        q43.A2="2k + 1";
        q43.A3="2k + 2";
        q43.A4="2k – 1";
        q43.RA="2k – 1";
        array.add(q43);
        q44.q="Suppose you have coins of denominations 1, 3 and 4. You use a greedy algorithm, in which you choose the largest denomination coin which is not greater than the remaining sum.\\n For which of the following sums, will the algorithm NOT produce an optimal answer?";
        q44.A1="20";
        q44.A2="12";
        q44.A3="6";
        q44.A4="5";
        q44.RA="6";
        array.add(q44);

        q45.q="Suppose you have coins of denominations 1, 3 and 4. You use a greedy algorithm, in which you choose the largest denomination coin which is not greater than the remaining sum.\\n For which of the following sums, will the algorithm NOT produce an optimal answer?";
        q45.A1="20";
        q45.A2="12";
        q45.A3="6";
        q45.A4="5";
        q45.RA="6";
        array.add(q45);

        insert();


        Button A11 = findViewById(R.id.A1);
        A11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button button1 =findViewById(R.id.A1);
                if(button1.getText()==array.get(count).RA)
                    {
                        Toast.makeText(getApplicationContext(), " Correct", Toast.LENGTH_SHORT).show();
                        price();
                        }
                    else
                    {

                        Toast.makeText(getApplicationContext(), " Incorrect", Toast.LENGTH_SHORT).show();
                        lose();

                    }


            }
        });
        Button A22 = findViewById(R.id.A2);
        A22.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Button button2 =findViewById(R.id.A2);
                    if(button2.getText()==array.get(count).RA)
                    {
                        Toast.makeText(getApplicationContext(), " Correct", Toast.LENGTH_SHORT).show();
                        price();
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(), " Incorrect", Toast.LENGTH_SHORT).show();

                       lose();
                    }
                }
        });
        Button A33 = findViewById(R.id.A3);
        A33.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button button3 = findViewById(R.id.A3);

                if (button3.getText() == array.get(count).RA)
                {
                    Toast.makeText(getApplicationContext(), " Correct", Toast.LENGTH_SHORT).show();

                    price();
                }
                else
                    {

                    Toast.makeText(getApplicationContext(), " Incorrect", Toast.LENGTH_SHORT).show();
                    lose();
                    }
            }
        });
        Button A44 = findViewById(R.id.A4);
        A44.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button button4 =findViewById(R.id.A4);
                if(button4.getText()==array.get(count).RA)
                    {
                        Toast.makeText(getApplicationContext(), " Correct", Toast.LENGTH_SHORT).show();
                       price();
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(), " Incorrect", Toast.LENGTH_SHORT).show();
                        lose();
                    }
            }
        });

    }




}

