package com.example.m.myapplication;

import android.app.Activity;
import android.os.Bundle;
import android.util.DisplayMetrics;

public class Pop14 extends Activity {
    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.popwindow14);

        DisplayMetrics dm = new DisplayMetrics() ;
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels ;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width *0.8) ,(int)(height*0.6));
    }
}
